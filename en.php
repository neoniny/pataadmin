<?php
$aun['name']="Patama Ramchune";
$aun['taxid']="TaxId: 1529900292336";
$aun['addr']="Address: 270/10 Life Rachadapisek Huaikwang Huaikwang Bangkok 10310";
$aun['tel']="Telephone: 063-9928247, 086-6707174";
$aun['email']="Email: patama.ram@gmail.com";

$docInfo['title']['1']="Quotation";
$docInfo['title']['2']="Invoice";
$docInfo['title']['3']="Receipt";

$docInfo['name']="Customer name";
$docInfo['taxid']="TaxId";
$docInfo['addr']="Address";
$docInfo['tel']="Telephone";
$docInfo['date']="Date";
$docInfo['docNo']="Document No.";


$tableInfo['title']="Item Description";
$tableInfo['item']="Item";
$tableInfo['detail']="Description";
$tableInfo['price']="Price";
$tableInfo['totalitem']="Total";
$tableInfo['currency']="THB";
$tableInfo['vat']="Vat 3%";
$tableInfo['total']="Sub total";
$tableInfo['net']="Total";

$paymentInfo['title']="Payment Information";
$paymentInfo['name']="Patama Ramchune";
$paymentInfo['accountno']="Account No.";
$paymentInfo['scb']="Siam Commercial Bank (SCB)";
$paymentInfo['ktb']="Krungthai Bank (KTB)";
$paymentInfo['kbank']="Kasikorn Bank (KBANK)";
$paymentInfo['promptpay']="Promptpay";
$paymentInfo['reciever']="Reciever";

?>