<div class="form-group text-left">
	<button type="submit" class="btn btn-primary" name="<?= $_GET['action'] ?>">บันทึก</button>
</div>
	    <div class="row form-group text-left">
			<div class="col-md-4">
	            <label for="company_name">ชื่อบริษัท: </label>
	        </div>
	        <div class="col-md-8">
	            <input required class="form-control" id="company_name" name="company_name" type="text" placeholder="ชื่อบริษัท" value="<?=$company_name?>">
	        </div>
	    </div>
	    <div class="row form-group text-left">
			<div class="col-md-4">
	            <label for="tax_id">เลขประจำตัวผู้เสียภาษี: </label>
	        </div>
	        <div class="col-md-8">
	            <input class="form-control" id="tax_id" name="tax_id" type="text" placeholder="เลขประจำตัวผู้เสียภาษี" value="<?=$tax_id?>" pattern="\d{13}">
	        </div>
	    </div>

	    <div class="row form-group text-left">
			<div class="col-md-4">
	            <label for="telephone">โทรศัพท์: </label>
	        </div>
	        <div class="col-md-8">
	            <input class="form-control" id="telephone" name="telephone" type="text" placeholder="โทรศัพท์" value="<?=$telephone?>" pattern="\d*">
	        </div>
	    </div>
	    <div class="row form-group text-left">
	    	<div class="col-md-4">
	    		<label for="addr">ที่อยู่ภาษาไทย</label>
	    	</div>
	    	<div class="col-md-8">
	    		<textarea class="form-control" name="addr_th" value="<?=$addr_th?>"><?=$addr_th?></textarea>
	    	</div>
	    </div>
	    <div class="row form-group text-left">
	    	<div class="col-md-4">
	    		<label for="addr">ที่อยู่ภาษาอังกฤษ</label>
	    	</div>
	    	<div class="col-md-8">
	    		<textarea class="form-control" name="addr_en" value="<?=$addr_en?>"><?=$addr_en?></textarea>
	    	</div>
	    </div>
	    <div class="row form-group text-left">
			<div class="col-md-4">
	            <label for="email">Email: </label>
	        </div>
	        <div class="col-md-8">
	            <input class="form-control" name="email" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Email" value="<?php echo $email; ?>">
	        </div>
	    </div>

