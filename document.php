<?php include('_session_login.php');?>
<?php include('document_controller.php');?>
<?php include('_header.php');?>
        <!-- Sidebar -->
        <?php include('_sidebar.php');?>
        <!-- End of Sidebar -->
        <!-- Topbar -->
        <?php include('_topbar.php');?>
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <?php
                if ($_GET["type"] == '1'){
                    $docType = "ใบเสนอราคา";
                } 
                if ($_GET["type"] == '2'){
                    $docType = "ใบแจ้งหนี้";
                } 
                if ($_GET["type"] == '3'){
                $_REQUEST['id'] = $_GET['id'];
                    $docType = "ใบเสร็จรับเงิน";
                }
            ?>
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">เอกสาร<?=$docType?></h1>

            </div>
<?php
  if (isset($_SESSION['success'])){
    ?>
<div class="alert alert-success alert-dismissible">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <?= $_SESSION['success'] ?>
</div>
    <?php
    unset($_SESSION['success']);
  }
?>
            <?php 
                if ($_GET["action"] == 'create'){
                    include('document/create.php');
                } 
                if ($_GET["action"] == 'inquiry'){
                    include('document/inquiry.php');
                } 
                if ($_GET["action"] == 'edit'){
                $_REQUEST['id'] = $_GET['id'];
                    include('document/edit.php');
                } 
                if ($_GET["action"] == 'view'){
                    include('document/view.php');
                } 
            ?>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
    <?php include('_footer.php');?>
    <!-- Scroll to Top Button-->
<?php include('_additional_js.php');?>

</body>

</html>