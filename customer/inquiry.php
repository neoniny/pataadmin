<div class="mb-3 ml-3">
  <a href="customer.php?action=create" class="buttonlink"><i class="fa fa-plus-circle"></i> เพิ่มรายชื่อลูกค้า</a>
</div>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> รายชื่อลูกค้า</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>เลขที่</th>
                  <th>ชื่อ - นามสกุล</th>
                  <th></th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>เลขที่</th>
                  <th>ชื่อ - นามสกุล</th>
                  <th></th>
                </tr>
              </tfoot>
              <tbody>
                <?php
                while ($row = mysqli_fetch_array($result)) {
                  ?>
                <tr>
                  <td><?= $row['id']?></td>
                  <td><?php 
                  	echo $row['company_name'];
                  	?>
                  	</td>
                  <td>
                    <a href="customer.php?action=edit&id=<?= $row['id']?>"><i class="fa fa-edit"></i></i></a>
                    <a href="customer.php?action=delete&id=<?= $row['id']?>"><i class="fa fa-trash-o"></i></a></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ต้องการลบหรือไม่</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="modal-btn-si">ลบ</button>
        <button type="button" class="btn btn-primary" id="modal-btn-no">ยกเลิก</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalConfirm = function(callback){
  
  $("#btn-confirm").on("click", function(){
    $("#mi-modal").modal('show');
  });

  $("#modal-btn-si").on("click", function(){
    callback(true);
    $("#mi-modal").modal('hide');
  });
  
  $("#modal-btn-no").on("click", function(){
    callback(false);
    $("#mi-modal").modal('hide');
  });
};
</script>