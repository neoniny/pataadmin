<?php
include('server.php') ;
$today = new \DateTime( 'now');
$current_date = date_format($today, 'Y-m-d');
$current_year = date_format($today,'y');

// view
if ($_GET["action"] == 'inquiry'){
    $type = $_GET['type'];
    $reports = mysqli_query($db,"SELECT * FROM `report` WHERE `type` = '$type' AND deleted_at IS NULL");
    $customers = mysqli_query($db,"SELECT * FROM `customer`");
    // mysqli_close($db);
}

// create init
if ($_GET["action"] == 'create'){
	$type = $_REQUEST['type'];
    $id = $_REQUEST['quoteid'];
    $date_issue = $current_date;
    $customers = mysqli_query($db,"SELECT * FROM `customer`");

    if($type==2 or $type ==3){
        $query = "SELECT * FROM `report` where `id`='$id'";
        $reports = mysqli_fetch_assoc(mysqli_query($db,$query));
        $item_query = "SELECT * FROM `report_item` where `report_id`='$id'";
        $items = mysqli_query($db,$item_query);
        $date_issue = $reports['date'];
    }
}

// create
if (isset($_POST['create'])) {

	$company_id = $_POST['company_id'];
    $type       = $_GET['type'];
    $date_issue   = (string)$_POST['date_issue'];
    $total = $_POST['total'];
    $items = $_POST['item'];
    $vat = $_POST['vat'];

    // Auto generate invoice_code
    if ($type == 1){
        $txtType = "Q";
    }elseif ($type == 2) {
        $txtType = "I";
    }else{
        $txtType = "R";
    }

    if ($vat ==1){
        $vat = $total*0.03;
    }else{
        $vat = 0;
    }
    // Q21001
    $lastId_query = "SELECT report_id FROM report WHERE `type` = $type ORDER BY report_id DESC LIMIT 1";
    $lastId_result = mysqli_fetch_assoc(mysqli_query($db, $lastId_query));

    $lastId = $lastId_result['report_id'];
    $lastId = str_split($lastId, 3);
    $last_year = $lastId[0][1].$lastId[0][2];

    $report_id = "";
    if(strcmp($current_year,$last_year) == 0){
        $currentId = (int)$lastId[1]+1;
        $report_id = $txtType.(string)$last_year.str_pad($currentId, 3, "0", STR_PAD_LEFT); 
    }else{
        $report_id = $txtType.(string)$current_year."001";
    }

	// created by
	$username = $_SESSION['username'];
	$userId_query = "SELECT `id` FROM `users` WHERE `username`='$username'";
	$userId_result = mysqli_fetch_assoc(mysqli_query($db, $userId_query));
	$userId = $userId_result['id'];

    $create_query = "INSERT INTO `report`(`report_id`, `customer_id`, `type`, `date`, `total`, `vat`, `created_at`, `created_by`) VALUES ('$report_id',$company_id,$type,'$date_issue','$total',$vat,CURRENT_TIMESTAMP,'$userId');";
    $result = mysqli_query($db, $create_query);

    $report_id = mysqli_insert_id($db);
    if (!$report_id){
        $_SESSION['success'] = "บันทึกไม่ได้ $create_query";
        echo "<script>location.href='document.php?action=create&type=".$type."';</script>";
        return 0;
    }
    
    foreach ($items as $key => $item) {
        $info = mysqli_real_escape_string($db, $_POST[$item.'item_info']);
        $amount = mysqli_real_escape_string($db, $_POST[$item.'item_amount']);
        $item_query = "INSERT INTO `report_item` (`report_id`, `detail`, `price`) VALUES ('$report_id', '$info', '$amount');";
        mysqli_query($db, $item_query);
    }

    if ($type == 2){
        $invoice_status = "INSERT INTO `invoice_status`(`invoice_id`, `status`, `create_date`,`total`) VALUES ($report_id,'Pending',CURRENT_TIMESTAMP,'$total')";
        $result = mysqli_query($db, $invoice_status);
    }elseif ($type==3) {
        $quoteid=$_GET['quoteid'];
        $invoice_status = "UPDATE `invoice_status` SET `invoice_id`=$quoteid,`recieve_id`=$report_id,`status`='Success',`update_date`=CURRENT_TIMESTAMP WHERE `invoice_id`=$quoteid";
        $result = mysqli_query($db, $invoice_status);
    }

    if ($type == 2 or $type == 3){
        $messege = "INSERT INTO `message`( `role`, `type`, `link_table`, `link_id`, `create_date`) VALUES (1,1,'report','$report_id',CURRENT_TIMESTAMP)";
        $result = mysqli_query($db, $messege);
    }

    $_SESSION['success'] = "บันทึกแล้ว";
    echo "<script>location.href='document.php?action=inquiry&type=".$type."';</script>";
    exit;
}

// edit init
if ($_GET["action"] == 'edit'){
    $type = $_REQUEST['type'];
    $id = $_REQUEST['id'];
    $customers = mysqli_query($db,"SELECT * FROM `customer`");
    $query = "SELECT * FROM `report` where `id`='$id'";
    $reports = mysqli_fetch_assoc(mysqli_query($db,$query));
    $item_query = "SELECT * FROM `report_item` where `report_id`='$id'";
    $items = mysqli_query($db,$item_query);
    $date_issue = $reports['date'];
}

if (isset($_POST['edit'])) {
    $id         = $_GET['id'];
    $type = $_GET['type'];
    $company_id = $_POST['company_id'];
    $type       = $_GET['type'];
    $date_issue   = (string)$_POST['date_issue'];
    $total = $_POST['total'];
    $items = $_POST['item'];
    $vat = $_POST['vat'];
    if ($vat ==1){
        $vat = $total*0.03;
    }else{
        $vat = 0;
    }

    $edit_query = "UPDATE `report` SET `customer_id` = '$company_id', `type` = '$type', `date` = '$date_issue', `total` = '$total',`vat` = $vat,`updated_at` = CURRENT_TIMESTAMP WHERE `report`.`id` = $id;";
    mysqli_query($db, $edit_query);

    // remove item information
    $items = $_POST['item'];
    $removeItem  = "DELETE from `report_item` where `report_id` = $id";
    mysqli_query($db, $removeItem);

    foreach ($items as $key => $item) {
        $info = mysqli_real_escape_string($db, $_POST[$item.'item_info']);
        $amount = mysqli_real_escape_string($db, $_POST[$item.'item_amount']);
        $item_query = "INSERT INTO `report_item` (`report_id`, `detail`, `price`) VALUES ('$id', '$info', '$amount');";
        mysqli_query($db, $item_query);
    }
    $_SESSION['success'] = "แก้ไขแล้ว";

    echo "<script>location.href='document.php?action=inquiry&type=".$type."';</script>";
    exit;
}

//Delete
if ($_GET["action"] == 'delete') {
    $id = $_GET['id'];
    $type = $_GET["type"];

    $delete_query = "UPDATE `report` SET `deleted_at` = CURRENT_TIMESTAMP where `id`='$id'";
    mysqli_query($db, $delete_query);
    $removeItem  = "DELETE from `report_item` where `report_id` = $id";
    mysqli_query($db, $removeItem);

    if ($type == 2 or $type == 3){
        $message = "INSERT INTO `message`( `role`, `type`, `link_table`, `link_id`, `create_date`) VALUES (1,2,'report','$id',CURRENT_TIMESTAMP)";
         var_dump($messege);
        $result = mysqli_query($db, $message);
    }


    $_SESSION['success'] = "รายการที่ท่านเลือกได้ถูกลบไปแล้ว";
    echo "<script>location.href='document.php?action=inquiry&type=".$type."';</script>";
    exit;



}

?>