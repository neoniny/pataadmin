<?php
$customerArray = [];
while ($row = mysqli_fetch_array($customers)) {
  $customerArray[$row['id']]=$row['company_name'];
}
?>
<div class="mb-3 ml-3">
  <a href="document.php?action=create&type=<?=$_GET['type']?>" class="buttonlink"><i class="fa fa-plus-circle"></i>สร้างเอกสาร<?= $docType?></a>
</div>

      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> รายงานเอกสาร<?= $docType?></div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>หมายเลขเอกสาร</th>
                  <th>วันที่</th>
                  <th>ชื่อลูกค้า</th>
                  <th></th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>หมายเลขเอกสาร</th>
                  <th>วันที่</th>
                  <th>ชื่อลูกค้า</th>
                  <th></th>
                </tr>
              </tfoot>
              <tbody>
                <?php
                while ($row = mysqli_fetch_array($reports)) {
                  ?>
                <tr>
                  <td><?= $row['report_id']?></td>
                  <td><?= $row['date']?></td>
                  <td><?= $customerArray[$row['customer_id']]?>
                    <?php
                      if($_GET['type']==1){
                        echo '<a href="document.php?action=create&quoteid='.$row['id'].'&type=2"><i class="fa fa-file-invoice"></i></i></a></td>';
                      }elseif ($_GET['type']==2) {
                        echo '<a href="document.php?action=create&quoteid='.$row['id'].'&type=3"><i class="fa fa-file-invoice"></i></i></a></td>';
                      }
                    ?>
                  <td>
                    <a href="document_pdf.php?id=<?= $row['id']?>" target="_blank"><i class="fa fa-print"></i></a>
                    <a href="document.php?action=edit&id=<?= $row['id']?>&type=<?=$_GET["type"]?>"><i class="fa fa-edit"></i></i></a>
                    <a href="document.php?action=delete&id=<?= $row['id']?>&type=<?=$_GET["type"]?>"><i class="fa fa-trash"></i></a></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ต้องการลบหรือไม่</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="modal-btn-si">ลบ</button>
        <button type="button" class="btn btn-primary" id="modal-btn-no">ยกเลิก</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalConfirm = function(callback){
  
  $("#btn-confirm").on("click", function(){
    $("#mi-modal").modal('show');
  });

  $("#modal-btn-si").on("click", function(){
    callback(true);
    $("#mi-modal").modal('hide');
  });
  
  $("#modal-btn-no").on("click", function(){
    callback(false);
    $("#mi-modal").modal('hide');
  });
};
</script>