<?php
$aun['name']="น.ส.ปฐมา แรมชื่น";
$aun['taxid']="เลขประจำตัวผู้เสียภาษี: 1529900292336";
$aun['addr']="ที่อยู่: 270/10 ไลฟ์รัชดาภิเษก แขวงห้วยขวาง เขตห้วยขวาง กทม. 10310";
$aun['tel']="โทรศัพท์: 063-9928247, 086-6707174";
$aun['email']="อีเมล์: patama.ram@gmail.com";

$docInfo['title']['1']="ใบเสนอราคา";
$docInfo['title']['2']="ใบวางบิล";
$docInfo['title']['3']="ใบเสร็จรับเงิน";

$docInfo['name']="ชื่อลูกค้า";
$docInfo['taxid']="เลขประจำตัวผู้เสียภาษี";
$docInfo['addr']="ทีี่อยู่";
$docInfo['tel']="โทรศัพท์";
$docInfo['date']="วันที่";
$docInfo['docNo']="เอกสารเลขที่";


$tableInfo['title']="รายละเอียดบริการ";
$tableInfo['item']="รายการ";
$tableInfo['detail']="รายละเอียดบริการ";
$tableInfo['price']="ราคา";
$tableInfo['totalitem']="รวมทั้งหมด";
$tableInfo['currency']="บาท";
$tableInfo['all']="ทั้งหมด";
$tableInfo['vat']="ภาษี 3%";
$tableInfo['total']="จำนวนเงินที่ได้รับ";
$tableInfo['net']="ยอดรวมทั้งหมด";

$paymentInfo['title']="รายละเอียดการชําระเงิน";
$paymentInfo['name']="ปฐมา แรมชื่น";
$paymentInfo['accountno']="หมายเลขบัญชี";
$paymentInfo['scb']="ธนาคารไทยพาณิชย์:";
$paymentInfo['ktb']="ธนาคารกรุงไทย:";
$paymentInfo['kbank']="ธนาคารกสิกรไทย:";
$paymentInfo['promptpay']="พร้อมเพย์:";
$paymentInfo['reciever']="ผู้รับเงิน";

?>