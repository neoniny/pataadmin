<?php
require('fpdf.php');
require('alphapdf.php');
include('server.php') ;
include('_session_login.php');
define('FPDF_FONTPATH','font/');

$id = $_REQUEST['id'];
$lang = $_REQUEST['lang'];

$query = "SELECT * FROM `report` where `id`='$id'";
$report = mysqli_fetch_assoc(mysqli_query($db,$query));
$query = "SELECT * FROM `customer` where `id`=".$report['customer_id'];
$customer = mysqli_fetch_assoc(mysqli_query($db,$query));

$item_query = "SELECT * FROM `report_item` where `report_id`='$id'";
$items = mysqli_query($db,$item_query);

if ($_GET["action"] == 'updateread'){
    $userid = $_GET["user"];
    $msgid = $_GET["msgid"];
    $query_update_msg="REPLACE INTO `message_read`(`message_id`, `user`, `read`) VALUES ($msgid,$userid,CURRENT_TIMESTAMP)";
    $result = mysqli_query($db,$query_update_msg);

}

class PDF extends FPDF
{
    function thai_date($billing_date){   // 19 ธันวาคม 2556 เวลา 10:10:43
        $monthTH = ['ธันวาคม','มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];  
        $date = strtotime($billing_date);
        $thai_date_return = date("d",$date);   
        $thai_date_return.=" ".$monthTH[(int)date("m",$date)];   
        $thai_date_return.= " ".(date("Y",$date)+543);   
        return $thai_date_return;   
    }

    function english_date($billing_date){   // 19 ธันวาคม 2556 เวลา 10:10:43
        $date = strtotime($billing_date);
        $dateObj   = DateTime::createFromFormat('!m', date("m",$date));
        $monthName = $dateObj->format('F');

        $date_return = date("d",$date);   
        $date_return.=" ".$monthName;   
        $date_return.= " ".(date("Y",$date));   
        return $date_return;   
    }

    function Header()
        {
        global $aun;

        // Logo
        $this->AddFont('saraban','','THSarabunNew.php');
        $this->SetFont('saraban','',14);

        $arrangeL = 20;
        $this->SetX($arrangeL);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$aun['name']), 0, 0, '' ); 
        $this->SetX($arrangeL);
        $this->Cell(0,12,iconv( 'UTF-8','TIS-620',$aun['taxid']),0,0,'');
        $this->SetX($arrangeL);
        $this->Cell(0,24,iconv( 'UTF-8','TIS-620',$aun['addr']),0,0,'');
        $this->SetX($arrangeL);
        $this->Cell(0,36,iconv( 'UTF-8','TIS-620',$aun['tel']),0,0,'');
        $this->SetX($arrangeL);
        $this->Cell(0,48,iconv( 'UTF-8','TIS-620',$aun['email']),0,0,'');
        $this->Ln(40);
    }

    function info(){
        global $report;
        global $customer;
        global $docInfo;
        global $date_issue;

        $this->AddFont('saraban','','THSarabunNew.php');
        $this->AddFont('saraban','B','THSarabunNew Bold.php');
        
        $arrangeL = 20;
        $this->SetX($arrangeL);
        $this->SetFont('saraban','B',24);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$docInfo['title'][$report['type']]), 0, 0, 'C' ); 
        $this->Ln(10);
        $this->SetFont('saraban','',16);
        $this->SetX($arrangeL);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$docInfo['docNo'].": ".$report['report_id']), 0, 0, 'R' ); 
        $this->Ln(7);
        $this->SetX($arrangeL);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$docInfo['date'].": ".$date_issue), 0, 0, 'R' ); 
        $this->Ln(7);
        $this->SetX($arrangeL);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$docInfo['name'].": ".$customer['company_name']), 0, 0, '' );
        $this->Ln(7);
        $this->SetX($arrangeL);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$docInfo['taxid'].": ".$customer['tax_id']), 0, 0, '' );
        $this->Ln(7);
        $this->SetX($arrangeL);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$docInfo['addr'].": ".$customer['addr']), 0, 0, '' );
        $this->Ln(7);
        $this->SetX($arrangeL);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$docInfo['tel'].": ".$customer['telephone']), 0, 0, '' );
        $this->Ln(10);
    }

    function tableInfo(){
        global $report;
        global $tableInfo;
        global $items;

        $this->AddFont('saraban','','THSarabunNew.php');
        $this->AddFont('saraban','B','THSarabunNew Bold.php');
        
        $arrangeL = 20;
        $this->SetX($arrangeL);
        $this->SetFont('saraban','B',20);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$tableInfo['title']), 0, 0, '' );
        $this->SetFont('saraban','',14);
        $this->Ln(10);
        $this->SetX($arrangeL);
        $item1=20;
        $item2=120;
        $item3=40;
        $this->Cell($item1,0,iconv( 'UTF-8','TIS-620',$tableInfo['item']),0,0,C);
        $this->Cell($item2,0,iconv( 'UTF-8','TIS-620',$tableInfo['detail']),0,0,C);
        $this->Cell($item3,0,iconv( 'UTF-8','TIS-620',$tableInfo['price']),0,0,C);

        $this->Ln();
        $this->SetX(15);
        //Vertical Line
        $this->SetDrawColor(77,77,77);
        
        $colStart=20;
        $col2 = $colStart+$item1;
        $col3 = $col2+$item2;
        $col4 = $col3+$item3;
        $colStop = 200;
        $rowStart=110;
        $row2=107;

        // Item detail information
        $item =1;
        $lineHeigth=5;
        $this->Ln($lineHeigth);
        $lastRow=$this->GetY();
        $total = 0;
        foreach ($items as $key => $row) {
            $this->SetY($lastRow+$lineHeigth);
            $price = (float)$row['price'];
            $currentRow= $this->GetY();
            $this->SetX($arrangeL);
            $this->Cell($item1,0,$item,0,0,C);
            $this->infoBody($item2,$row['detail'],$currentRow,$item1+10,14);
            $lastRow = $this->GetY();
            $this->SetY($currentRow);
            $this->SetX($col4);
            $this->Cell(0,0,iconv('UTF-8','TIS-620',number_format($price,2)." ".$tableInfo['currency']),0,0,R);
            $total=$total+$price;
            $this->SetY($lastRow);
            $item++;
        }
        $allItem =$item-1;
        $currentRow= $this->GetY();
        $tableLength = $currentRow+$lineHeigth;
        $this->Line($colStart,$tableLength,$colStop,$tableLength);
        $this->Line($col2,$rowStart,$col2,$tableLength);
        
        $this->SetFont('saraban','B',14);
        $this->Ln(10);
        $this->SetX($arrangeL);
        $this->Cell($item1+$item2,0,iconv( 'UTF-8','TIS-620',$tableInfo['all']." ".$allItem." ".$tableInfo['item']),0,0,R);


        if($report['type']==3){
            $vat = $report['vat'];
            if($vat>0){
                $subTotal = $total - $vat;
                $this->Ln(7);
                $this->SetX($arrangeL);
                $this->Cell($item1+$item2,0,iconv( 'UTF-8','TIS-620',$tableInfo['total']),0,0,R);
                $this->Cell($item3,0,iconv( 'UTF-8','TIS-620',number_format($subTotal,2)." ".$tableInfo['currency']),0,0,R);
                $this->Ln(7);
                $this->SetX($arrangeL);
                $this->Cell($item1+$item2,0,iconv( 'UTF-8','TIS-620',$tableInfo['vat']),0,0,R);
                $this->Cell($item3,0,iconv( 'UTF-8','TIS-620',number_format($vat,2)." ".$tableInfo['currency']),0,0,R);
            }
            $this->Ln(7);
            $this->SetX($arrangeL);
            $this->Cell($item1+$item2,0,iconv( 'UTF-8','TIS-620',$tableInfo['net']),0,0,R);
            $this->Cell($item3,0,iconv( 'UTF-8','TIS-620',number_format($total,2)." ".$tableInfo['currency']),0,0,R);
        }else{
            $this->Ln(7);
            $this->SetX($arrangeL);
            $this->Cell($item1+$item2,0,iconv( 'UTF-8','TIS-620',$tableInfo['totalitem']),0,0,R);
            $this->Cell($item3,0,iconv( 'UTF-8','TIS-620',number_format($total,2)." ".$tableInfo['currency']),0,0,R);
        }
        

        $currentRow= $this->GetY();
        $tableLength = $currentRow+$lineHeigth;
        $titleHeigth = 11;

        $this->Line($colStart,$rowStart,$colStart,$tableLength);
        $this->Line($colStart,$rowStart,$colStop,$rowStart);
        $this->Line($colStop,$rowStart,$colStop,$tableLength);
        $this->Line($colStart,$tableLength,$colStop,$tableLength);
        
        $this->Line($col3,$rowStart,$col3,$tableLength);
        $this->Line($colStart,$rowStart+$titleHeigth,$colStop,$rowStart+$titleHeigth);
        $this->Ln(10);
        
    }

    function paymentInfo(){
        global $paymentInfo;
        $accountNo['scb']="111-289047-8";
        $accountNo['ktb']="029-0-10630-3";
        $accountNo['kbank']="004-3-66022-5";
        $accountNo['promptpay']="0866707174";

        $arrangeL = 20;
        $this->Ln(10);
        $this->SetX($arrangeL);
        $this->SetFont('saraban','B',14);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$paymentInfo['title']), 0, 0, '' ); 
        $this->Ln(10);
        $this->SetFont('saraban','',14);
        $this->SetX($arrangeL);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$paymentInfo['scb']), 0, 0, '' ); 
        $this->Ln(5);
        $this->SetX($arrangeL+10);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$paymentInfo['name']." ".$paymentInfo['accountno']." ".$accountNo['scb']), 0, 0, '' );
        $this->Ln(7);
        $this->SetX($arrangeL);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$paymentInfo['ktb']), 0, 0, '' ); 
        $this->Ln(5);
        $this->SetX($arrangeL+10);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$paymentInfo['name']." ".$paymentInfo['accountno']." ".$accountNo['ktb']), 0, 0, '' );
        $this->Ln(7);
        $this->SetX($arrangeL);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$paymentInfo['kbank']), 0, 0, '' ); 
        $this->Ln(5);
        $this->SetX($arrangeL+10);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$paymentInfo['name']." ".$paymentInfo['accountno']." ".$accountNo['kbank']), 0, 0, '' );
        $this->Ln(7);
        $this->SetX($arrangeL);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$paymentInfo['promptpay']), 0, 0, '' ); 
        $this->Ln(5);
        $this->SetX($arrangeL+10);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$paymentInfo['name']." ".$paymentInfo['accountno']." ".$accountNo['promptpay']), 0, 0, '' );
        $this->Ln(7);
    }

    function recieveInfo(){
        global $paymentInfo;
        $arrangeL = 150;
        $this->Ln(20);
        $this->SetX($arrangeL);
        $this->SetFont('saraban','',14);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$paymentInfo['name']), 0, 0, 'C' );
        $this->Ln(7);
        $this->SetX($arrangeL);
        $this->Cell(0,0,iconv( 'UTF-8','TIS-620',$paymentInfo['reciever']), 0, 0, 'C' ); 

    }

    function infoBody($width,$txt,$top,$left,$fontsize)
        {
            // Font
            $this->SetFont('saraban','',$fontsize);
            // // Output text in a 6 cm width column
            $this->SetY($top-2);
            $this->Cell($left,0);
            $this->MultiCell($width,5,iconv( 'UTF-8','TIS-620',$txt),0,L);
        }

}
$pdf = new PDF();
if($lang=='en'){
    $lang = include('en.php');
    $customer['addr'] = $customer['addr_en'];
    $date_issue = $pdf->english_date($report['date']);
}else{
    $lang = include('th.php');
    $customer['addr'] = $customer['addr_th'];
    $date_issue = $pdf->thai_date($report['date']);
}

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->AddFont('saraban','','THSarabunNew.php');
$pdf->AddFont('saraban','B','THSarabunNew Bold.php');
$pdf->SetFont('saraban','B',30);
$pdf->info();
$pdf->tableInfo();
if($report['type']==1){
    $pdf->paymentInfo();
}elseif ($report['type']==2) {
    $pdf->paymentInfo();
}else{
    $pdf->recieveInfo();
}

$pdf->Output();

?>