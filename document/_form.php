<div class="form-group text-left">
	<button type="submit" class="btn btn-primary" name="<?= $_GET['action'] ?>">บันทึก</button>
</div>

<div class="row form-group text-left">
	<div class="col-md-4">
        <label for="company_name">ชื่อลูกค้า: </label>
    </div>
    <div class="col-md-8">
        <select required class="form-control" id="company_id" name="company_id" placeholder="ชื่อบริษัท">
        	<?php
    			while ($row = mysqli_fetch_array($customers)) {
    		?>
			  <option value="<?=$row['id']?>" <?php if($reports['customer_id']==$row['id']) echo "selected"?>><?=$row['company_name']?></option>
			<?php }?>
		</select>
    </div>
</div>
<div class="row form-group text-left">
	<div class="col-md-4">
        <label for="date_issue">วันที่ออกเอกสาร: </label>
    </div>
    <div class="col-md-8">
        <input type="date" class="form-control" id="date_issue" name="date_issue" value="<?=$date_issue?>">
    </div>
</div>

<div class="row form-group text-left">
	<div class="col-md-4">
		<label for="total">ยอดเงินทั้งหมด: </label>
	</div>
	<div class="col-md-8">
		<input class="form-control" id="total" name="total" type="text" placeholder="0" value="<?=$reports['total']?>">
	</div>
</div>
<div class="row form-group text-left">
	<div class="col-md-4">
		<label for="total">ภาษี 3%: </label>
	</div>
	<div class="col-md-8">
		<input class="form" type="checkbox" value="1" id="vat" name="vat" checked>
	</div>
</div>
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">รายละเอียดรายการ</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="line_item" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>รายละเอียด</th>
                  <th>ราคา</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
				<?php 
			  		if ($_GET["action"] == 'edit' OR isset($_GET["quoteid"])) {
			  			$index = 1;
			  			while ($row = mysqli_fetch_array($items)) { 
			  				$item_info = $row['detail'];
			  				$item_amount = $row['price'];
			  	?>
                <tr id="item<?=$index?>">
					<td>
						<input id="item" name="item['<?=$index?>']" type="hidden" value="<?=$index?>">
						<textarea class="form-control" name="<?=$index?>item_info" rows="1"><?= $item_info?></textarea>
					</td>
					<td>
						<input class="form-control" name="<?=$index?>item_amount" type="text" onfocusout="validateMoney('<?=$index?>item_amount')" value="<?= $item_amount?>">
					</td>
					<td>
						<button id="remove" class="btn btn-default"><i class="fa fa-trash"></i></button>
					</td>
				</tr>
				<?php 
						$index = $index+1;
						}
						$initIndex = $index;
			  		}
			  		else {
			  			$initIndex = 1;
			  		}
			  	?>
              </tbody>
            </table>
        </div>
    </div>
</div>
  
<script>
	$(document).ready(function(){

		$index = <?= $initIndex ?>;
	    $('#addmore').click(function(){
	    	console.log("click!");
	        var item = $('input[name=additem]').val();
	        $("#line_item").append('<tr id="item'+$index+'"><td><input id="item" name="item[\''+$index+'\']" type="hidden" value="'+$index+'"><textarea class="form-control" name="'+$index+'item_info" rows="1"></textarea></td><td><input class="form-control" name="'+$index+'item_amount" type="text" onfocusout="validateMoney(\''+$index+'item_amount\')" value="0"></td><td><button id="remove" class="btn btn-default"><i class="fa fa-trash"></i></button></td></tr>');

	        $index=$index+1;
	        $(document).ready(function () {
		        $("input").keypress(function (e) {
		            if (e.which == 13) {
		            	console.log("this");
		                event.preventDefault();
		                $(this).blur();
		            }
		        });
		    });
	    });
	    $("#remove").click(function(){
	       $(this).parent('tr').remove();
	    });
    	$(document).ready(function () {
	        $("input").keypress(function (e) {
	            if (e.which == 13) {
	                event.preventDefault();
	                $(this).blur();
	            }
	        });
	    });
	    // check total
	});
	
	$('#line_item').on('click', 'button', function(){
		$(this).closest('tr').remove();
		calTotal();
	});

	function validateMoney($txtBoxName){
		var $val = document.querySelectorAll('[name="'+$txtBoxName+'"]')[0];
		if (!$.isNumeric( $val.value ) || ($val.value <= 0 ) ) {
	        $('[name="'+$txtBoxName+'"]').val(0);
	        console.log("กรุณาใส่จำนวนสินค้าเป็นตัวเลข และมากกว่า 0 เท่านั้น")
	        $('#message').append('<div class="alert alert-danger alert-dismissible inner-message"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>กรุณาใส่จำนวนสินค้าเป็นตัวเลข และมากกว่า 0 เท่านั้น</div>')
	    }
	    calTotal();
	}

	function calTotal(){
		$total = 0;
		$('input[name^="item"]').each(function() {
			$item = $(this).val();
			$item_amount = $('[name="'+$item+'item_amount"]').val();
			$total = Number($total)+Number($item_amount);
		});
		$('input[name="total"]').val($total);
	}
</script>