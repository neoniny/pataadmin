<?php include('_session_login.php');?>
<?php include('customer_controller.php');?>
<?php include('_header.php');?>
        <!-- Sidebar -->
        <?php include('_sidebar.php');?>
        <!-- End of Sidebar -->
        <!-- Topbar -->
        <?php include('_topbar.php');?>
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">ข้อมูลลูกค้า</h1>
            </div>
<?php
  if (isset($_SESSION['success'])){
    ?>
<div class="alert alert-success alert-dismissible">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <?= $_SESSION['success'] ?>
</div>
    <?php
    unset($_SESSION['success']);
  }
?>
            <?php 
            if ($_GET["action"] == 'create'){
                include('customer/create.php');
            } 
            if ($_GET["action"] == 'inquiry'){
                include('customer/inquiry.php');
            } 
            if ($_GET["action"] == 'edit'){
            $_REQUEST['id'] = $_GET['id'];
                include('customer/edit.php');
            } 
            if ($_GET["action"] == 'view'){
                include('customer/view.php');
            } 

            ?>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
    <?php include('_footer.php');?>
    <!-- Scroll to Top Button-->
<?php include('_additional_js.php');?>

</body>

</html>