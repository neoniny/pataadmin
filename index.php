<?php include('_session_login.php');?>
<?php include('_header.php');?>
<?php
  if (isset($_SESSION['success'])){
    ?>
<div class="alert alert-success alert-dismissible">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <?= $_SESSION['success'] ?>
</div>
    <?php
    unset($_SESSION['success']);
  }
?>

        <!-- Sidebar -->
        <?php include('_sidebar.php');?>
        <!-- End of Sidebar -->



                <!-- Topbar -->
                <?php include('_topbar.php');?>
                <!-- End of Topbar -->
                
                <?php include('_dashboard.php');?>

                <?php include('_footer.php');?>

    <!-- Scroll to Top Button-->
<?php include('_additional_js.php');?>

</body>

</html>