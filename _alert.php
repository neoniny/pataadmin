<?php
include('server.php') ;
// หา role ของ currentuser
// created by
$username = $_SESSION['username'];
$userId_query = "SELECT `role`,`id`  FROM `users` WHERE `username`='$username'";
$user_result = mysqli_fetch_assoc(mysqli_query($db, $userId_query));
$role = $user_result['role'];

$query_msg =  "SELECT m.id, m.type, m.link_table, m.link_id, m.create_date, r.read FROM message m LEFT JOIN message_read r ON m.id = r.message_id WHERE m.role = 1 ORDER BY `create_date` DESC LIMIT 5";
$msg = mysqli_query($db,$query_msg);
$unread = 0;
foreach ($msg as $item) {
    if(is_null($item['read'])){
        $unread = $unread+1;
    }
}

?>

<!-- Nav Item - Alerts -->
        <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts -->
                <span class="badge badge-danger badge-counter"><?=$unread?></span>
            </a>
            <!-- Dropdown - Alerts -->
            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                    Alerts Center
                </h6>
                <?php
                    foreach ($msg as $item) {
                        $detail = "";
                        if($item['type']==1){
                            $severity = "bg-primary";
                            $action = "สร้างขึ้นแล้ว";
                        }elseif ($item['type']==2) {
                            $severity = "bg-warning";
                            $action = "ลบแล้ว";
                        }

                        if ($item['link_table']=='report'){
                            $link = 'document_pdf.php?id='.$item['link_id']."&user=".$user_result['id']."&msgid=".$item['id'];
                            if(is_null($item['read'])){
                                $spanclass = 'class="font-weight-bold"';
                                $link = $link."&action=updateread";
                            }elseif ($item['type']==2) {
                                $link="#";
                            }
                            else{
                                $spanclass = "";
                            }

                            $query_report = "SELECT r.type, c.company_name FROM report r JOIN customer c ON r.customer_id = c.id WHERE r.id = ".$item['link_id'];
                            $result = mysqli_fetch_assoc(mysqli_query($db, $query_report));
                            if($result['type'] == 2){
                                $type="ใบวางบิล";
                            }else{
                                $type="ใบเสร็จรับเงิน";
                            }
                            $detail = "เอกสาร".$type." สำหรับ".$result['company_name']."ได้ถูก".$action;
                            $icon = "fa-file-alt";
                        }elseif ($item['link_table']=='customer') {
                            $detail = "ลูกค้า";
                        }
                ?>
                    <a class="dropdown-item d-flex align-items-center" 
                        href="<?=$link?>">
                        <div class="mr-3">
                            <div class="icon-circle <?=$severity?>">
                                <i class="fas <?= $icon ?> text-white"></i>
                            </div>
                        </div>
                        <div>
                            <div class="small text-gray-500"><?= $item['create_date'] ?></div>
                            <span <?=$spanclass?>><?=  $detail ?></span>
                        </div>
                    </a>
                <?php
                    }
                ?>
                
                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
            </div>
        </li>