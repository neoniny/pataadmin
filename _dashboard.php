<?php 
include('server.php') ;
$today = new \DateTime( 'now');
$current_date = date_format($today, 'Y-m-d');
$current_year = date_format($today,'Y');
$current_month = date_format($today,'m');
$color_chart = ["#FFB5E8","#B28DFF","#DCD3FF","#AFF8DB","#BFFCC6","#FFC9DE","FF9CEE","C5A3FF"];

$query_pending = "SELECT COUNT(`invoice_id`) FROM `invoice_status` WHERE `status`='Pending'";
$pending = mysqli_fetch_assoc(mysqli_query($db,$query_pending))["COUNT(`invoice_id`)"];
$query_success = "SELECT COUNT(`invoice_id`) FROM `invoice_status` where `status`='Success'";
$success = mysqli_fetch_assoc(mysqli_query($db,$query_success))["COUNT(`invoice_id`)"];

$success_percent = 100*($success/($success+$pending));

$earn_month_sql = "SELECT MONTH(update_date) as 'month',SUM(total) AS total FROM invoice_status WHERE (status = 'Success') AND (update_date BETWEEN '".$current_year."-01-01' AND '".$current_year."-12-31') GROUP BY MONTH(update_date) ORDER BY MONTH(update_date)";
$earn_month = mysqli_query($db,$earn_month_sql);

$monthly = [0,0,0,0,0,0,0,0,0,0,0,0,0];
$annual = 0;
foreach ($earn_month as $month ) {
	$monthly[$month["month"]]=$month["total"];
	$annual = $annual+$month["total"];
}

$earn_customer_sql = "SELECT r.customer_id, r.total FROM invoice_status i JOIN report r ON i.recieve_id = r.id WHERE i.status = 'Success'";
$earn_customer = mysqli_query($db,$earn_customer_sql);

$all_customer_sql= "SELECT id, company_name FROM customer";
$all_customer = mysqli_query($db,$all_customer_sql);
// digaram by money
$customer_money = [];
foreach ($earn_customer as $customer ) {
	if(!$customer_money[$customer["customer_id"]]){
		$customer_money[$customer["customer_id"]] = [floatval($customer['total']),1];
	}else{
		$thiscustomer = $customer_money[$customer["customer_id"]];
		$customer_money[$customer["customer_id"]] = [floatval($thiscustomer[0])+floatval($customer['total']),$thiscustomer[1]+1];
	}
}
$customer_name = [];
foreach ($all_customer as $customer) {
	$customer_name[$customer['id']]=$customer['company_name'];
}

?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
    </div>

    <!-- Content Row -->
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Earnings (Monthly)</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$monthly[intval($current_month)]?>THB</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                Earnings (Annual)</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$annual?> THB</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Earn
                            </div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?=$success_percent?>%</div>
                                </div>
                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                        <div class="progress-bar bg-info" role="progressbar"
                                            style="width: <?=$success_percent?>%" aria-valuenow="50" aria-valuemin="0"
                                            aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pending Requests Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                Pending Requests</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$pending?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->

    <div class="row">

        <!-- Area Chart -->
        <div class="col-xl-12 col-lg-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Earnings Overview</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="myAreaChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
    	<!-- Pie Chart -->
        <div class="col-xl-4 col-lg-5">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Revenue Sources (THB)</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-pie pt-4 pb-2">
                        <canvas id="myPieChart"></canvas>
                    </div>
                    <div class="mt-4 text-center small">
                    	<?php
							foreach ($customer_money as $key => $value) {
								echo '<span class="mr-2"><i class="fas fa-circle" style="color: '
								.$color_chart[$key].' "></i>'.$customer_name[$key].'</span>';
							}
                    	?>
                    </div>
                </div>
            </div>
        </div>
    	<!-- Pie Chart -->
        <div class="col-xl-4 col-lg-5">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Revenue Sources (Taks)</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-pie pt-4 pb-2">
                        <canvas id="myPieChart_task"></canvas>
                    </div>
                    <div class="mt-4 text-center small">
                    	<?php
							foreach ($customer_money as $key => $value) {
								echo '<span class="mr-2"><i class="fas fa-circle" style="color: '
								.$color_chart[$key].' "></i>'.$customer_name[$key].'</span>';
							}
                    	?>
                    </div>
                </div>
            </div>
        </div>
    </div>

	</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->



    <!-- Page level plugins -->
    <script src="vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <!-- <script src="js/demo/chart-area-demo.js"></script> -->
    <script type="text/javascript">
     <?php 
        array_shift($monthly);
    ?>
        var $dataMonth = [<?=implode($monthly,',')?>];
        var customer_name=[];
        var customer_money=[];
        var customer_work=[];
        var color_chart=[];
     <?php
     	foreach ($customer_money as $key => $value) {
     		echo "customer_name.push('".$customer_name[$key]."');";
     		echo "customer_money.push('".$value[0]."');";
     		echo "customer_work.push('".$value[1]."');";
     		echo "color_chart.push('".$color_chart[$key]."');";
     	}
     ?> 

    </script>
    <script src="js/chart-area.js"></script>

    <script src="js/chart-pie.js"></script>

