CREATE TABLE `customer` (
  `id` int(11) UNSIGNED NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `tax_id` varchar(13) DEFAULT NULL,
  `addr_en` varchar(1000) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `addr_th` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `report` (
  `id` int(11) UNSIGNED NOT NULL,
  `report_id` varchar(100) NOT NULL,
  `customer_id` int(11) UNSIGNED NOT NULL,
  `type` varchar(1) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `vat` decimal(11,2) DEFAULT NULL,
  `total` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) UNSIGNED NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `report_item` (
  `report_id` int(11) UNSIGNED NOT NULL,
  `detail` varchar(1000) NOT NULL,
  `price` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`) VALUES
(1, 'pataadmin', '7d3a695d8f5cf0cc4675959383ed6b0f', 'neoniny@gmail.com'),
(2, 'pataaccount', '7d3a695d8f5cf0cc4675959383ed6b0f', 'patama.ram@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `report_item`
--
ALTER TABLE `report_item`
  ADD KEY `report_id` (`report_id`);

ALTER TABLE `message_read`
  ADD KEY `message` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;


ALTER TABLE `message`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- Constraints for dumped tables
--
SELECT * FROM message_read r JOIN message m ON r.message_id = m.id

--
-- Constraints for table `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `report_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);

ALTER TABLE `message`
  ADD CONSTRAINT `role_message` FOREIGN KEY (`role`) REFERENCES `role` (`id`);

ALTER TABLE users
  ADD CONSTRAINT users_role
  FOREIGN KEY (role)
  REFERENCES role(id);


ALTER TABLE message_read
  ADD CONSTRAINT read_message_user
  FOREIGN KEY (message_id)
  REFERENCES message(id);
--
-- Constraints for table `report_item`
--
ALTER TABLE `report_item`
  ADD CONSTRAINT `report_item_ibfk_1` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`);


